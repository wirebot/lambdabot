FROM ubuntu:16.04

RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y curl \
		wget \
		make \
		git \
		python-software-properties \
		sudo \
		vim-tiny \
    zsh \
    python \
    python-setuptools \
    zlib1g-dev \
    libncurses-dev \
    libpcre3-dev \
    libreadline-dev \
    libgmp-dev \
    gcc
# install packages
# RUN apt-fast.sh -y install \
# 	libdrm-dev \
# 	libdrm-nouveau2=2.4.43-0ubuntu0.0.3 \
# 	libkms1=2.4.43-0ubuntu0.0.3 \
# 	libdrm-intel1=2.4.43-0ubuntu0.0.3 \
# 	libdrm-nouveau1a=2.4.43-0ubuntu0.0.3 \
# 	libdrm-radeon1=2.4.43-0ubuntu0.0.3 \
# 	libdrm2=2.4.43-0ubuntu0.0.3 \
# 	&& apt-fast.sh -y install \
# 		cabal-install \
# 		libncurses-dev \
# 		libpcre3-dev \
# 		libreadline-dev \
# 		zlib1g-dev
# RUN apt-get update
# RUN apt-get install -y zlib1g-dev libncurses-dev libpcre3-dev libreadline-dev
RUN wget https://haskell.org/platform/download/8.0.1/haskell-platform-8.0.1-unknown-posix--full-x86_64.tar.gz
RUN tar xvf haskell-platform-8.0.1-unknown-posix--full-x86_64.tar.gz && ./install-haskell-platform.sh
RUN apt-get install -y libgmp-dev

# NOTE(wting|2014-07-20): ignored due to parent image env var:
# 	ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_FRONTEND interactive
ENV PATH /root/.cabal/bin:$PATH

RUN cabal update \
	&& cabal install cabal-install
RUN cabal install djinn
RUN cabal install --only-dependencies lambdabot-5.1
RUN cabal install haskell-src-exts-simple-1.18.0.1.1
RUN cabal install hoogle-5.0.4
RUN hoogle generate
# To allow preserving state between runs:
RUN mkdir $HOME/.lambdabot
COPY lambdaserv /lambdaserv
COPY lambdabot-core /lambdabot-core
RUN cd /lambdabot-core && cabal install --force-reinstalls
COPY lambdabot-reference-plugins /lambdabot-reference-plugins
RUN cd /lambdabot-reference-plugins && cabal install --force-reinstalls
COPY lambdabot-haskell-plugins /lambdabot-haskell-plugins
RUN cd /lambdabot-haskell-plugins && cabal install
COPY lambdabot-social-plugins /lambdabot-social-plugins
RUN cd /lambdabot-social-plugins && cabal install
COPY lambdabot-novelty-plugins /lambdabot-novelty-plugins
RUN cd /lambdabot-novelty-plugins && cabal install
COPY lambdabot-irc-plugins /lambdabot-irc-plugins
RUN cd /lambdabot-irc-plugins && cabal install
COPY lambdabot-misc-plugins /lambdabot-misc-plugins
RUN cd /lambdabot-misc-plugins && cabal install
COPY lambdabot-chalmers-plugins /lambdabot-chalmers-plugins
RUN cd /lambdabot-chalmers-plugins && cabal install
COPY lambdabot /lambdabot
RUN cd /lambdabot && cabal install
# RUN git clone https://github.com/dschoepe/vt.git /vt
# RUN apt-get install -y python3-setuptools
# RUN cd /vt && python3 setup.py install
# COPY init.rc /
ENV LANG C.UTF-8
EXPOSE 8012
CMD /lambdaserv
