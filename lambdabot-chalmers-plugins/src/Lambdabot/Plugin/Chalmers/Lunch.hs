module Lambdabot.Plugin.Chalmers.Lunch where

import Control.Applicative
import Control.Monad
import Network.HTTP
import Text.HTML.TagSoup
import Data.Time.Calendar.WeekDate
import Data.Time.LocalTime
import Data.Time.Format
import Data.Char
import Data.List.Split

import Lambdabot.Plugin
import Lambdabot.Util

type WeekMenu = [(String, [String])]

lunchPlugin :: Module ()
lunchPlugin = newModule
  { moduleCmds = return
      $ [ (command "lunch")
            { help = say "Show today's lunch menu"
            , aliases = ["elluncho"]
            , process = lunch }
        , (command "einstein")
            { help = say "@einstein [day]: Show Einstein menu for the week or specified day."
            , process = einsteinCmd }
        , (command "ooto")
            { help = say "@ooto [day]: Show OOTO menu for week or specified day."
            , process = ootoCmd }
        ]
  }


lunch rest = do
  idx <- io todaysIdx
  selFun <- io $ getSelFun rest (\m -> (:[]) $ elt m idx ("", [noLunchMsg]))
  let getItm = unlines . snd . head . selFun
  ein <- io einstein
  say "*Einstein*: "
  say . getItm =<< io einstein
  say "*OOTO*: "
  say . getItm =<< io ooto

sayMenu rest menu = do
  selFun <- io $ getSelFun rest id
  let menu' = selFun menu
  forM_ menu' $ \(day, itms) ->
    say ("*" ++ day ++ "*: " ++ unlines itms)

einsteinCmd rest = sayMenu rest =<< io einstein
ootoCmd rest = sayMenu rest =<< io ooto

getSelFun :: String -> (WeekMenu -> WeekMenu) -> IO (WeekMenu -> WeekMenu)
getSelFun s defaultFun = do
  idx <- todaysIdx
  return . go idx . map toLower . trim $ s
  where go tIdx x | Just idx <- weekdayName tIdx x =
                                \menu -> [elt menu idx
                                           ("Invalid", [noLunchMsg])]
                  | otherwise = defaultFun
        weekdayName tIdx x
          | Just i <- lookup x (zip (map (map toLower) weekdays) [0..]) = Just i
          | x == "today" = Just tIdx
          | otherwise = Nothing

weekdays :: [String]
weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]

openURL :: String -> IO String
openURL x = getResponseBody =<< simpleHTTP (getRequest x)

sanitize :: String -> String
sanitize = trim . concatMap go
  where go '\160' = ""
        go '\194' = ""
        go '\n' = " "
        go x = [x]

trim :: String -> String
trim = reverse . dropWhile isSpace . reverse . dropWhile isSpace

elt :: [a] -> Int -> a -> a
elt xs i def | length xs > i && i >= 0 = xs !! i
             | otherwise = def

dropLast = reverse . drop 1 . reverse

einstein :: IO WeekMenu
einstein = do
  tags <- parseTags <$> openURL "http://www.butlercatering.se/einstein"
  let relTags = dropWhile (~/= "<div class=\"field-day\">") tags
  let parts = sections (~== "<div class=\"field-day\">") relTags
  let days = flip map parts $ \p' ->
        let p = dropWhile (~/= "<h3 class=\"field-label\">") p'
        in (innerText (take 2 p), dropLast . filter ((/= "") . sanitize) .
                                  map (sanitize . innerText . (:[])) .
                                  takeWhile (~/= "</div>") $ drop 2 p)
  return days

ooto :: IO WeekMenu
ooto = do
  tags <- parseTags <$> openURL "http://ooto.se/sv/lunchmeny/"
  let divClass = "<div class=\"avia_textblock texto_menu_2\""
      relTags = dropWhile (~/= divClass) tags
      parts = sections (~== divClass) relTags
      items = map (sanitize . innerText . takeWhile (~/= "</section>")) parts
  return $ zip weekdays (chunksOf 3 items)


noLunchMsg :: String
noLunchMsg = "No lunch today"

todaysIdx :: IO Int
todaysIdx = do
  zonTime <- getZonedTime
  return (subtract 1 $ (read (formatTime defaultTimeLocale "%w" zonTime)) :: Int)

todaysItems :: WeekMenu -> IO [String]
todaysItems menu = do
  dayNum <- todaysIdx
  if dayNum < 0 || dayNum >= 6
    then return ["no lunch today"]
    else return . snd $ elt menu dayNum ("Invalid", [noLunchMsg])

einsteinToday :: IO [String]
einsteinToday = todaysItems =<< einstein

ootoToday :: IO [String]
ootoToday = todaysItems =<< ooto
