module Lambdabot.Plugin.Chalmers
    (chalmersPlugins, lunchPlugin
    ) where

import Lambdabot.Plugin.Chalmers.Lunch

chalmersPlugins :: [String]
chalmersPlugins = ["lunch"]
